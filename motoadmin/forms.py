from django.forms import ModelForm
from .models import CounterParty, Order, OrderItem, Product


class CounterPartyForm(ModelForm):
    class Meta:
        model = CounterParty
        exclude = ('balance_usd', 'balance_uzs', 'created_at')


class OrderForm(ModelForm):
    class Meta:
        model = Order
        exclude = ('created_at', 'status', 'user', 'total_uzs', 'total_usd', 'rate')


class OrderEditForm(ModelForm):
    class Meta:
        model = Order
        exclude = ('created_at', )


class OrderItemForm(ModelForm):
    class Meta:
        model = OrderItem
        exclude = ('created_at', 'rate')


class ProductForm(ModelForm):
    class Meta:
        model = Product
        exclude = ('created_at', 'count')
