from django.urls import path, include
from django.contrib.auth.decorators import login_required
from . import views


workers_url = [
    path('', login_required(views.WorkersView.as_view()), name='workers_list'),
    path('add/', login_required(views.WorkersAddView.as_view()), name='workers_add'),
    path('edit/<int:pk>', login_required(views.WorkersEditView.as_view()), name='workers_edit'),
]


cars_url = [
    path('', login_required(views.CarsView.as_view()), name='cars_list'),
    path('add/', login_required(views.CarsAddView.as_view()), name='cars_add'),
    path('edit/<int:pk>', login_required(views.CarsEditView.as_view()), name='cars_edit'),
]


costs_url = [
    path('', login_required(views.CostsView.as_view()), name='costs_list'),
    path('add_cost/', login_required(views.CostsCreateView.as_view()), name='costs_add'),
    path('edit/<int:pk>', login_required(views.CostsCategoryEditView.as_view()), name='costs_cat_edit'),
    path('list_costs/<int:pk>', login_required(views.CostsCategoryAddView.as_view()), name='costs_cat_list'),
    path('list_add/<int:pk>', login_required(views.CostsCategoryUpdateView.as_view()), name='costs_cat_add'),
    path('add_edit/<int:pk>', login_required(views.CostsCategoryInAddEditView.as_view()),
         name='costs_cat_after_radd_edit'),
    path('costs_lists/', login_required(views.CostsAddView.as_view()), name='costs1_list'),
    path('costs_lists_add/', login_required(views.CostsListsAddView.as_view()), name='costs1_list_add'),
    path('costs_return_lists/', login_required(views.CostsReturnView.as_view()), name='costs_return_list'),
]


ajax_urls = [
    path('get_category/', login_required(views.ajax_category_outlay), name='ajax_get_category'),
    path('get_workers/', login_required(views.ajax_get_workers), name='ajax_get_workers'),
    path('get_cars/', login_required(views.ajax_get_cars), name='ajax_get_cars'),
]


income_urls = [
    path('', login_required(views.IncomeView.as_view()), name='income_list'),
    path('add/', login_required(views.IncomeAddView.as_view()), name='income_list_add'),
    path('each/<int:pk>', login_required(views.IncomeEachView.as_view()), name='income_each'),
    path('each_add/<int:pk>', login_required(views.IncomeEachAddView.as_view()), name='income_each_add'),
    path('close/<int:pk>', login_required(views.close_income), name='income_each_close'),
    path('end/<int:pk>', login_required(views.end_income), name='income_each_end'),
    path('delete/<int:pk>', login_required(views.delete_income), name='income_each_delete'),
]


store_urls = [
    path('', login_required(views.StoreView.as_view()), name='store_list'),
    path('add/', login_required(views.StoreAddView.as_view()), name='store_add'),
    path('edit/<int:pk>', login_required(views.StoreEditView.as_view()), name='store_edit'),
]


urlpatterns = [
    path('cars/', include(cars_url)),
    path('workers/', include(workers_url)),
    path('costs/', include(costs_url)),
    path('ajax/', include(ajax_urls)),
    path('income', include(income_urls)),
    path('store/', include(store_urls)),
]
