from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib.auth import authenticate
from django.views import View
from django.views.generic import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth import login, logout
from django.http import HttpResponse
from .models import Product, CounterParty, Order, OrderItem
from .forms import CounterPartyForm, OrderItemForm
from .utils import render_to_pdf


class GeneratePdf(View):
    def get(self, request, *args, **kwargs):
        pdf = render_to_pdf('pdf/invoice.html')
        return HttpResponse(pdf, content_type='application/pdf')


def logout_view(request):
    logout(request)
    return redirect('/')


class LoginView(View):
    def get(self, request):
        if request.user.is_authenticated:
            return redirect('product:list')
        return render(request, 'login.html')

    def post(self, request):
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user:
            login(request, user)
            return redirect('product:list')
        return render(request, 'login.html')


class ProductListVIew(ListView):
    model = Product
    template_name = 'product.html'


class AddProductCreateView(CreateView):
    model = Product
    fields = ('title', 'size', 'price', 'status', 'code')
    template_name = 'product_add.html'
    success_url = reverse_lazy('product:list')


class ProductUpdateView(UpdateView):
    model = Product
    fields = ('title', 'size', 'price', 'status', 'code')
    template_name = 'product_edit.html'
    success_url = reverse_lazy('product:list')


class OrderView(ListView):
    model = Order
    template_name = 'order.html'


class OrderCreateView(CreateView):
    model = Order
    fields = ('counter_party', )
    template_name = 'order_add.html'
    success_url = reverse_lazy('order:list')


class OrderUpdateView(UpdateView):
    model = Order
    fields = ('counter_party', 'status', 'user', 'total_uzs', 'total_usd', 'rate')
    template_name = 'order_edit.html'
    success_url = reverse_lazy('order:list')


class OrderDetailView(DetailView):
    model = Order
    template_name = 'order_each.html'

    def get_context_data(self, **kwargs):
        context = super(OrderDetailView, self).get_context_data(**kwargs)
        context['product'] = Product.objects.all()
        context['order_item'] = OrderItem.objects.all()
        context['form'] = OrderItemForm()
        return context


class OrderItemCreateView(CreateView):
    model = OrderItem
    form_class = OrderItemForm
    success_url = reverse_lazy('order:list')

    # def get_success_url(self, *args, **kwargs):
    #     return HttpResponse(self.request.path_info)
    # HELP
    def form_valid(self, form, **kwargs):
        order = Order.objects.get(id=self.kwargs['pk'])
        counter_party = CounterParty.objects.get(id=order.counter_party_id)
        d = form.save(commit=False)
        if form.cleaned_data['description']:
            d.description = str(form.cleaned_data['description']) + 'Оплата за доставку'
        else:
            d.description = 'Оплата за доставку'
        if not form.cleaned_data['for_delivery']:
            if form.cleaned_data['currency'] == 'Доллар':
                counter_party.balance_usd += int(form.cleaned_data['sum'])
            else:
                counter_party.balance_uzs += int(form.cleaned_data['sum'])
            counter_party.save()
            if form.cleaned_data['description']:
                d.description = str(form.cleaned_data['description']) + 'Оплата за заказ'
            else:
                d.description = 'Оплата за заказ'
        d.save()
        return super().form_valid(form)


def order_action(request, pk):
    status = int(request.POST.get('status'))
    Order.objects.filter(id=pk).update(status=status)
    return redirect(request.META['HTTP_REFERER'])


class CounterPartyView(ListView):
    model = CounterParty
    template_name = 'counter_party.html'


class CounterPartyCreateView(CreateView):
    model = CounterParty
    form_class = CounterPartyForm
    template_name = 'counter_party_add.html'
    success_url = reverse_lazy('counter-party:list')


class CounterPartyEachView(DetailView):
    model = CounterParty
    template_name = 'counter_party_each.html'


class CounterPartyUpdateView(UpdateView):
    model = CounterParty
    form_class = CounterPartyForm
    template_name = 'counter_party_edit.html'
    success_url = reverse_lazy('counter-party:list')


class IncomeView(ListView):
    model = OrderItem
    template_name = 'income.html'


class OutComeView(View):
    def get(self, request):
        context = {

        }
        return render(request, 'outcome.html', context)


def error_404(request, exception):
    return render(request, '404.html')
