from django.apps import AppConfig


class Motoadmin1Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'motoadmin1'
