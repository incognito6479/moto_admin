# Generated by Django 3.2.4 on 2021-06-28 15:35

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Car',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('car_number', models.CharField(max_length=200)),
                ('driver', models.CharField(max_length=200)),
                ('driver_phone', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Cashier',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.FloatField()),
                ('payment_type', models.CharField(max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='CounterParty',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=500)),
                ('balance_uzs', models.FloatField(blank=True, default=0, null=True)),
                ('balance_usd', models.FloatField(blank=True, default=0, null=True)),
                ('org_name', models.CharField(max_length=500)),
                ('phone', models.CharField(max_length=500)),
                ('phone2', models.CharField(blank=True, max_length=500, null=True)),
                ('address', models.CharField(blank=True, max_length=500, null=True)),
                ('inn', models.CharField(blank=True, max_length=500, null=True)),
                ('email', models.CharField(blank=True, max_length=200, null=True)),
                ('status', models.IntegerField(choices=[(1, 'Активный'), (2, 'Неактивный'), (3, 'Удаленный')])),
                ('sms', models.CharField(blank=True, max_length=500, null=True)),
                ('extra_info', models.CharField(blank=True, max_length=1000, null=True)),
                ('created_at', models.DateTimeField(default=datetime.datetime(2021, 6, 28, 15, 35, 34, 320514, tzinfo=utc))),
            ],
        ),
        migrations.CreateModel(
            name='Income',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_created=True)),
                ('status', models.BigIntegerField()),
                ('total_uzs', models.FloatField()),
                ('total_usd', models.FloatField()),
                ('rate', models.FloatField(blank=True, null=True)),
                ('comments', models.TextField(blank=True, null=True)),
                ('counter_party', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='motoadmin.counterparty')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(default=datetime.datetime(2021, 6, 28, 15, 35, 34, 321099, tzinfo=utc))),
                ('status', models.IntegerField(choices=[(1, 'Удалён'), (2, 'Закрытый'), (3, 'Оплаченный/Долговой '), (4, 'Завершенный'), (5, 'В процессе')], default=5)),
                ('total_uzs', models.FloatField(blank=True, default=0, null=True)),
                ('total_usd', models.FloatField(blank=True, default=0, null=True)),
                ('rate', models.FloatField(blank=True, default=0, null=True)),
                ('counter_party', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='motoadmin.counterparty')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='OrderItem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sum', models.FloatField(verbose_name='Сумма')),
                ('currency', models.CharField(choices=[(1, 'Доллар'), (2, 'Сум')], max_length=200, verbose_name='Валюта')),
                ('payment_type', models.CharField(choices=[(1, 'Наличные'), (2, 'Перечисление'), (3, 'Карта')], max_length=500, verbose_name='Метод оплаты')),
                ('for_delivery', models.BooleanField(default=False, verbose_name='За доставку?')),
                ('created_at', models.DateTimeField(default=datetime.datetime(2021, 6, 28, 15, 35, 34, 322031, tzinfo=utc))),
                ('description', models.CharField(max_length=500, verbose_name='Комментарии')),
                ('rate', models.FloatField(blank=True, default=0, null=True)),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='motoadmin.order')),
            ],
        ),
        migrations.CreateModel(
            name='OutLayCategory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=500)),
                ('type', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_created=True)),
                ('title', models.CharField(max_length=500)),
                ('size', models.CharField(max_length=500)),
                ('price', models.FloatField()),
                ('code', models.CharField(max_length=500)),
                ('count', models.IntegerField(default=0)),
                ('status', models.CharField(max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='ProjectSettings',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rate', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Workers',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_created=True)),
                ('name', models.CharField(max_length=200)),
                ('phone', models.CharField(max_length=200)),
                ('work_price', models.IntegerField()),
                ('work_place', models.CharField(max_length=500)),
                ('code', models.CharField(max_length=200)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='PaymentLog',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_created=True)),
                ('amount', models.FloatField()),
                ('payment_type', models.CharField(max_length=200)),
                ('payment_method', models.CharField(max_length=200)),
                ('outcat', models.IntegerField()),
                ('outlay', models.IntegerField()),
                ('outlay_child', models.IntegerField()),
                ('comment', models.TextField()),
                ('aor', models.BooleanField()),
                ('amount_type', models.CharField(max_length=500)),
                ('payment_type_log', models.CharField(max_length=500)),
                ('rate', models.FloatField()),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='OutLay',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_created=True)),
                ('name', models.CharField(max_length=500)),
                ('outlay_type_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='motoadmin.outlaycategory')),
            ],
        ),
        migrations.CreateModel(
            name='OrderReturnItem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_created=True)),
                ('count', models.FloatField()),
                ('status', models.IntegerField()),
                ('total_uzs', models.FloatField()),
                ('total_usd', models.FloatField()),
                ('rate', models.FloatField()),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='motoadmin.order')),
                ('order_item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='motoadmin.orderitem')),
            ],
        ),
        migrations.CreateModel(
            name='IncomeItem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('count', models.FloatField()),
                ('total_uzs', models.FloatField()),
                ('total_usd', models.FloatField()),
                ('price', models.FloatField()),
                ('currency', models.CharField(max_length=200)),
                ('income', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='motoadmin.income')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='motoadmin.product')),
            ],
        ),
    ]
