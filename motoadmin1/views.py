from django.shortcuts import render, redirect, HttpResponse
from django.views import View
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic import ListView, DetailView
from .models import Workers, Car, OutLayCategory, OutLay, Costs, Income, IncomeItem, Store
from .forms import StoreForm, WorkersForm, CarsForm, OutlayCategoryForms, OutlayForm, OutlayEditForm, CostsValidateForm, IncomeForm, IncomeItemForm
from django.core import serializers
from motoadmin.models import OrderItem, CounterParty, Product
from django.http import JsonResponse
import json


class WorkersView(ListView):
    model = Workers
    template_name = 'workers.html'


class WorkersAddView(CreateView):
    model = Workers
    form_class = WorkersForm
    template_name = 'workers_add.html'
    success_url = reverse_lazy('motoadmin1:workers_list')


class WorkersEditView(UpdateView):
    model = Workers
    form_class = WorkersForm
    template_name = 'workers_edit.html'
    success_url = reverse_lazy('motoadmin1:workers_list')


class CarsView(ListView):
    model = Car
    template_name = 'car.html'
    context_object_name = 'cars'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        context['form'] = CarsForm
        return context


class CarsAddView(CreateView):
    model = Car
    form_class = CarsForm
    success_url = reverse_lazy('motoadmin1:cars_list')


class CarsEditView(UpdateView):
    model = Car
    form_class = CarsForm
    template_name = 'cars_edit.html'
    success_url = reverse_lazy('motoadmin1:cars_list')


class CostsView(ListView):
    model = OutLayCategory
    template_name = 'costs.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        context['form'] = OutlayCategoryForms
        return context


class CostsCreateView(CreateView):
    model = OutLayCategory
    form_class = OutlayCategoryForms
    success_url = reverse_lazy('motoadmin1:costs_list')


class CostsCategoryEditView(UpdateView):
    model = OutLayCategory
    template_name = 'costs_category_edit.html'
    form_class = OutlayCategoryForms
    success_url = reverse_lazy('motoadmin1:costs_list')


class CostsCategoryAddView(ListView):
    model = OutLay
    template_name = 'costs_category_add.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['form'] = OutlayForm
        context['category_cost'] = OutLay.objects.filter(outlay_type_id=self.kwargs['pk'])
        context['pk'] = self.kwargs['pk']
        return context


class CostsCategoryUpdateView(CreateView):
    model = OutLay
    form_class = OutlayForm
    success_url = reverse_lazy('motoadmin1:costs_list')

    def form_valid(self, form, **kwargs):
        instance = OutLayCategory.objects.get(id=self.kwargs['pk'])
        f = form.save(commit=False)
        f.outlay_type_id = instance
        f.save()
        return super().form_valid(form)


class CostsCategoryInAddEditView(UpdateView):
    model = OutLay
    form_class = OutlayEditForm
    template_name = 'costs_cat_afteradd_edit.html'
    success_url = reverse_lazy('motoadmin1:costs_list')


class CostsAddView(ListView):
    model = Costs
    template_name = 'costs_add.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        context['cat'] = OutLayCategory.objects.all()
        return context


class CostsListsAddView(View):
    def post(self, request):
        form = CostsValidateForm(request.POST)
        if form.is_valid():
            f = form.save(commit=False)
            f.user = request.user
            f.save()
            # obj = Costs()
            # obj.category = self.request.POST.get('category')
            # obj.reason = self.request.POST.get('reason')
            # obj.changing_column = self.request.POST.get('changing_div')
            # obj.sum = self.request.POST.get('sum')
            # obj.comments = self.request.get('comment')
            # obj.currency = self.request.POST.get('currency')
            # obj.payment_type = self.request.POST.get('payment_type')
            # obj.user = self.request.user
            # obj.save()
        return redirect(request.META['HTTP_REFERER'])


class CostsReturnView(View):
    def get(self, request):
        obj = OrderItem.objects.all()
        cat = OutLayCategory.objects.all()
        context = {
            'obj': obj,
            'cat': cat,
        }
        return render(request, 'costs_return.html', context)

    def post(self, request):
        obj = OrderItem()
        form_validate = CostsValidateForm(request.POST)
        if form_validate.is_valid():
            obj.sum = request.POST['sum']
            obj.description = request.POST['comment']
            obj.currency = request.POST['currency']
            obj.payment_type = request.POST['payment_type']
            obj.user = request.user
            obj.save()
            return redirect(request.META['HTTP_REFERER'])
        return JsonResponse(form_validate.errors)


class IncomeView(ListView):
    model = Income
    template_name = 'income1.html'


class IncomeAddView(CreateView):
    model = Income
    form_class = IncomeForm
    template_name = 'income1_add.html'
    success_url = reverse_lazy('motoadmin1:income_list')


class IncomeEachView(DetailView):
    model = Income
    template_name = 'income1_each.html'
    context_object_name = 'obj'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['product'] = Product.objects.all()
        context['form'] = IncomeItemForm
        context['income_item'] = IncomeItem.objects.all()
        context['pk'] = self.kwargs['pk']
        return context
    # def post(self, request, pk):
    #     obj = Income.objects.get(id=pk)
    #     form = IncomeItemForm(request.POST)
    #     if form.is_valid():
    #         f = form.save(commit=False)
    #         counter_party = CounterParty.objects.get(id=obj.counter_party_id)
    #         if request.POST['currency'] == 'Доллар':
    #             counter_party.balance_usd -= int(request.POST['sum'])
    #         else:
    #             counter_party.balance_uzs -= int(request.POST['sum'])
    #         counter_party.save()
    #         f.income = obj
    #         f.save()
    #     return redirect(request.META['HTTP_REFERER'])


class IncomeEachAddView(CreateView):
    model = Income
    form_class = IncomeItemForm
    success_url = reverse_lazy('motoadmin1:income_list')

    def form_valid(self, form):
        obj = Income.objects.get(id=self.kwargs['pk'])
        f = form.save(commit=False)
        counter_party = CounterParty.objects.get(id=obj.counter_party_id)
        if form.cleaned_data['currency'] == 'Доллар':
            counter_party.balance_usd -= int(form.cleaned_data['sum'])
        else:
            counter_party.balance_uzs -= int(form.cleaned_data['sum'])
        counter_party.save()
        f.income = obj
        f.save()
        return super().form_valid(form)


class StoreView(ListView):
    model = Store
    template_name = 'store.html'
    context_object_name = 'store'


class StoreAddView(CreateView):
    model = Store
    template_name = 'store_add.html'
    form_class = StoreForm
    success_url = reverse_lazy('motoadmin1:store_list')


class StoreEditView(UpdateView):
    model = Store
    form_class = StoreForm
    template_name = 'store_edit.html'
    success_url = reverse_lazy('motoadmin1:store_list')


def ajax_category_outlay(request):
    data = json.loads(request.body)
    obj = serializers.serialize('json', OutLay.objects.filter(outlay_type_id=data['id']))
    return HttpResponse(obj)


def ajax_get_workers(request):
    obj = serializers.serialize('json', Workers.objects.all())
    return HttpResponse(obj)


def ajax_get_cars(request):
    obj = serializers.serialize('json', Car.objects.all())
    return HttpResponse(obj)


def close_income(request, pk):
    Income.objects.filter(id=pk).update(status='Закрыто')
    return redirect(request.META['HTTP_REFERER'])


def end_income(request, pk):
    Income.objects.filter(id=pk).update(status='Завершенный')
    return redirect(request.META['HTTP_REFERER'])


def delete_income(request, pk):
    Income.objects.filter(id=pk).delete()
    return redirect('motoadmin1:income_list')
