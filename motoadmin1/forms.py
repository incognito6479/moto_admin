from django import forms
from .models import Store, Income, IncomeItem, Workers, Car, OutLay, OutLayCategory, Costs


class StoreForm(forms.ModelForm):
    class Meta:
        model = Store
        fields = '__all__'


class IncomeForm(forms.ModelForm):
    class Meta:
        model = Income
        fields = ('counter_party', )


class IncomeItemForm(forms.ModelForm):
    class Meta:
        model = IncomeItem
        fields = ('currency', 'payment_type', 'sum', 'description')


class WorkersForm(forms.ModelForm):
    class Meta:
        model = Workers
        exclude = ('created_at', )


class CarsForm(forms.ModelForm):
    class Meta:
        model = Car
        fields = '__all__'


class OutlayCategoryForms(forms.ModelForm):
    class Meta:
        model = OutLayCategory
        fields = '__all__'


class OutlayForm(forms.ModelForm):
    class Meta:
        model = OutLay
        exclude = ('created_at', 'outlay_type_id')


class OutlayEditForm(forms.ModelForm):
    class Meta:
        model = OutLay
        exclude = ('created_at',)


class CostsValidateForm(forms.ModelForm):
    class Meta:
        model = Costs
        exclude = ('created_at',)


class CostsReturnValidateForm(forms.Form):
    sum = forms.FloatField()
    comments = forms.CharField(max_length=500, required=False)
    currency = forms.CharField(max_length=500)
    payment_type = forms.CharField(max_length=500)
