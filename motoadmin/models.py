from django.db import models
from django.contrib.auth.models import User
import datetime


PRODUCT_STATUS = (
    ('Активный', 'Активный'),
    ('Неактивный', 'Неактивный'),
)


class Product(models.Model):
    title = models.CharField(max_length=500)
    size = models.CharField(max_length=500)
    price = models.FloatField()
    status = models.CharField(max_length=500, choices=PRODUCT_STATUS)
    code = models.CharField(max_length=500, default=0, blank=True, null=True)
    created_at = models.DateTimeField(default=datetime.date.today)
    count = models.IntegerField(default=0, blank=True, null=True)

    def __str__(self):
        return f"{self.title} / {self.count}"


COUNTER_STATUS = (
    (1, "Активный"),
    (2, "Неактивный"),
    (3, "Удаленный"),
)


class CounterParty(models.Model):
    name = models.CharField(max_length=500)
    balance_uzs = models.FloatField(blank=True, null=True, default=0)
    balance_usd = models.FloatField(blank=True, null=True, default=0)
    org_name = models.CharField(max_length=500)
    phone = models.CharField(max_length=500)
    phone2 = models.CharField(max_length=500, blank=True, null=True)
    address = models.CharField(max_length=500, blank=True, null=True)
    inn = models.CharField(max_length=500, blank=True, null=True)
    email = models.CharField(max_length=200, blank=True, null=True)
    status = models.IntegerField(choices=COUNTER_STATUS)
    sms = models.CharField(max_length=500, blank=True, null=True)
    extra_info = models.CharField(max_length=1000, blank=True, null=True)
    created_at = models.DateTimeField(default=datetime.date.today)

    def __str__(self):
        return f"{self.name} / {self.org_name}"


ORDER_STATUS = (
    (1, "Удалён"),
    (2, "Закрытый"),
    (3, "Оплаченный/Долговой "),
    (4, "Завершенный"),
    (5, "В процессе"),
)


class Order(models.Model):
    created_at = models.DateTimeField(default=datetime.date.today)
    counter_party = models.ForeignKey(CounterParty, on_delete=models.CASCADE)
    status = models.IntegerField(choices=ORDER_STATUS, default=5)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    total_uzs = models.FloatField(default=0, blank=True, null=True)
    total_usd = models.FloatField(default=0, blank=True, null=True)
    rate = models.FloatField(default=0, blank=True, null=True)


PAYMENT_TYPE = (
    ('Наличные', 'Наличные'),
    ('Перечисление', 'Перечисление'),
    ('Карта', 'Карта'),
)
CURRENCY = (
    ('Доллар', 'Доллар'),
    ('Сум', 'Сум'),
)


class OrderItem(models.Model):
    sum = models.PositiveIntegerField(verbose_name='Сумма')
    currency = models.CharField(max_length=200, choices=CURRENCY, verbose_name='Валюта')
    payment_type = models.CharField(max_length=500, choices=PAYMENT_TYPE, verbose_name='Метод оплаты')
    for_delivery = models.BooleanField(default=False, verbose_name='За доставку?')
    created_at = models.DateTimeField(default=datetime.date.today)
    description = models.CharField(max_length=500, blank=True, null=True, verbose_name='Комментарии')
    rate = models.FloatField(blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)


class OrderReturnItem(models.Model):
    created_at = models.DateTimeField(auto_created=True)
    count = models.FloatField()
    status = models.IntegerField()
    order_item = models.ForeignKey(OrderItem, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    total_uzs = models.FloatField()
    total_usd = models.FloatField()
    rate = models.FloatField()




