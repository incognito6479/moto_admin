# Generated by Django 3.2.4 on 2021-06-28 15:35

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('motoadmin', '0002_auto_20210628_1535'),
    ]

    operations = [
        migrations.AlterField(
            model_name='counterparty',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 28, 15, 35, 51, 619173, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='order',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 28, 15, 35, 51, 619756, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 28, 15, 35, 51, 620689, tzinfo=utc)),
        ),
    ]
