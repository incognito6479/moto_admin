from django.urls import path, include
from django.contrib.auth.decorators import login_required
from . import views

counter_party_url = [
    path('', login_required(views.CounterPartyView.as_view()), name='list'),
    path('add/', login_required(views.CounterPartyCreateView.as_view()), name='add'),
    path('each/<int:pk>', login_required(views.CounterPartyEachView.as_view()), name='each'),
    path('update/<int:pk>', login_required(views.CounterPartyUpdateView.as_view()), name='update'),
]


order_urls = [
    path('', login_required(views.OrderView.as_view()), name='list'),
    path('add/', login_required(views.OrderCreateView.as_view()), name='add'),
    path('update/<int:pk>', login_required(views.OrderUpdateView.as_view()), name='update'),
    path('each/<int:pk>', login_required(views.OrderDetailView.as_view()), name='each'),
    path('action/<int:pk>', login_required(views.order_action), name='action'),
    path('add_order_item/<int:pk>', login_required(views.OrderItemCreateView.as_view()), name='order_item_add'),
]


income_urls = [
    path('', login_required(views.IncomeView.as_view()), name='list'),
]


product_urls = [
    path('', login_required(views.ProductListVIew.as_view()), name='list'),
    path('add/', login_required(views.AddProductCreateView.as_view()), name='add'),
    path('edit/<int:pk>', login_required(views.ProductUpdateView.as_view()), name='edit'),
]


urlpatterns = [
    path('', views.LoginView.as_view(), name='login'),
    path('logout/', login_required(views.logout_view), name='logout'),
    path('product/', include((product_urls, 'motoadmin'), namespace='product')),
    path('order/', include((order_urls, 'motoadmin'), namespace='order')),
    path('counter-party/', include((counter_party_url, 'motoadmin'), namespace='counter-party')),
    path('income/', include((income_urls, 'motoadmin'), namespace='income')),
    path('outcome/', login_required(views.OutComeView.as_view()), name='outcome'),
    path('pdf/', login_required(views.GeneratePdf.as_view()), name='pdf'),
]

handler404 = views.error_404
