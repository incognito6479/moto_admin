from django.db import models
from django.contrib.auth.models import User
from motoadmin import models as moto_admin_models
import datetime


class PaymentLog(models.Model):
    created_at = models.DateTimeField(auto_created=True)
    amount = models.FloatField()
    payment_type = models.CharField(max_length=200)
    payment_method = models.CharField(max_length=200)
    outcat = models.IntegerField()
    outlay = models.IntegerField()
    outlay_child = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    comment = models.TextField()
    aor = models.BooleanField()
    amount_type = models.CharField(max_length=500)
    payment_type_log = models.CharField(max_length=500)
    rate = models.FloatField()


class Workers(models.Model):
    name = models.CharField(max_length=200)
    phone = models.CharField(max_length=200)
    work_price = models.PositiveIntegerField()
    work_place = models.CharField(max_length=500)
    code = models.CharField(max_length=200)
    created_at = models.DateTimeField(default=datetime.date.today)


COSTS_TYPE = (
    ('Машина', 'Машина'),
    ('Сотрудники', 'Сотрудники'),
    ('Станки', 'Станки'),
)


class OutLayCategory(models.Model):
    name = models.CharField(max_length=500)
    type = models.CharField(max_length=500, choices=COSTS_TYPE)

    def __str__(self):
        return self.name


class OutLay(models.Model):
    created_at = models.DateTimeField(default=datetime.date.today)
    outlay_type_id = models.ForeignKey(OutLayCategory, on_delete=models.CASCADE)
    name = models.CharField(max_length=500)

    def __str__(self):
        return self.name


class Car(models.Model):
    car_number = models.CharField(max_length=200)
    driver = models.CharField(max_length=200)
    driver_phone = models.CharField(max_length=200)


class Cashier(models.Model):
    amount = models.FloatField()
    payment_type = models.CharField(max_length=500)


class ProjectSettings(models.Model):
    rate = models.FloatField()


CURRENCY = (
    ('Доллар', 'Доллар'),
    ('Сум', 'Сум'),
)


PAYMENT_TYPE = (
    ('Наличные', 'Наличные'),
    ('Перечисление', 'Перечисление'),
    ('Карта', 'Карта'),
)


class Costs(models.Model):
    category = models.CharField(max_length=500)
    changing_column = models.CharField(max_length=500, blank=True, null=True)
    reason = models.CharField(max_length=500)
    sum = models.FloatField()
    comments = models.CharField(max_length=500, blank=True, null=True)
    currency = models.CharField(max_length=500)
    payment_type = models.CharField(max_length=500)
    created_at = models.DateTimeField(default=datetime.date.today)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)


INCOME_STATUS = (
    ('Открыто', 'Открыто'),
    ('Закрыто', 'Закрыто'),
    ('Завершенный', 'Завершенный'),
)


class Income(models.Model):
    created_at = models.DateTimeField(default=datetime.date.today)
    total_uzs = models.FloatField(default=0.0, blank=True, null=True)
    total_usd = models.FloatField(default=0.0, blank=True, null=True)
    status = models.CharField(max_length=500, default='Открыто', choices=INCOME_STATUS)
    counter_party = models.ForeignKey(moto_admin_models.CounterParty, on_delete=models.CASCADE, verbose_name='Агент')


class IncomeItem(models.Model):
    created_at = models.DateTimeField(default=datetime.date.today)
    description = models.CharField(max_length=500, blank=True, null=True)
    sum = models.FloatField()
    income = models.ForeignKey(Income, on_delete=models.CASCADE)
    currency = models.CharField(max_length=500, choices=CURRENCY)
    payment_type = models.CharField(max_length=500, choices=PAYMENT_TYPE)


STORE_STATUS = (
    ('Активный', 'Активный'),
    ('Неактивный', 'Неактивный'),
)


class Store(models.Model):
    name = models.CharField(max_length=500)
    status = models.CharField(max_length=500, choices=STORE_STATUS)
