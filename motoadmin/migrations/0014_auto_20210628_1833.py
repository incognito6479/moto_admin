# Generated by Django 3.2.4 on 2021-06-28 18:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motoadmin', '0013_orderitem_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='code',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='status',
            field=models.CharField(choices=[('Активный', 'Активный'), ('Неактивный', 'Неактивный')], max_length=500),
        ),
    ]
